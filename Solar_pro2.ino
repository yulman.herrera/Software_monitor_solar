// Select your modem: Yulman Jose Herrera

#define TINY_GSM_MODEM_SIM800
#include <EEPROM.h>
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <String.h>
#include <LiquidCrystal_I2C.h> //Libreria LCD
#include "DHT.h"
#include <Wire.h> //I2C needed for sensors
#include "MPL3115A2.h" //Pressure sensor
#include "HTU21D.h" //Humidity sensor
#include <SPI.h>
#include <SD.h>
#include "RTClib.h"
#include "DHT.h"
#include <DallasTemperature.h> //DS18B20

OneWire oneWire6(6);
DallasTemperature sensors6(&oneWire6);

OneWire oneWire7(7);
DallasTemperature sensors7(&oneWire7);

OneWire oneWire9(10);
DallasTemperature sensors9(&oneWire9);

float temp;
float offset = 4;

//Lineas necesarias para el RTC
RTC_DS1307 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

//Lineas necesarias para la uSD
File myFile;

#define DHTTYPE DHT11
DHT dht4(4, DHTTYPE);
DHT dht5(5, DHTTYPE);

//=======================Lineas para el ACS712====================================//
int mVperAmp = 66; // use 100 for 20A Module and 66 for 30A Module
int RawValue= 0;
int ACSoffset = 2500;
unsigned long currentMillis = millis(), millis_loop = millis(), millis_SD = millis();
int a=0, b=0;
float count_main=0, count_sd=0;
float c=0;
 
double Voltage = 0;
double Amp = 0;
float ar;
float ar2;
float pir;

//=======================Lineas para el ACS712====================================//

int interval=1;
int cuenta=0;
String countx;
String ds18_temperatura6;
String ds18_temperatura7;
String ds18_temperatura9;
String WS;
String WD;
String humedad;
String temperatura;
String presion;
String humedad4;
String humedad5;
String temperatura4;
String temperatura5;
String corriente_load;
String Pan_propio;
String Pan_externo;
String Piranometro;
String current_board;
String peltier_v;
String peltier_c;

float aux_ds18_temp6=0;
float aux_ds18_temp7=0;
float aux_ds18_temp9=0;
float aux_WS=0;
float aux_WD=0;
float aux_pressure=0;
float aux_humidity=0;
float aux_tempf=0;
float aux_h4=0;
float aux_h5=0;
float aux_t4=0;
float aux_t5=0;
float aux_Amp=0;
float aux_ar=0;
float aux_ar2=0;
float aux_pir=0;
float aux_=0;
float aux_q1, aux_v;
float aux_peltv=0;
float aux_peltc=0;




int sd_freq=0;
float q=0, v=0;

//========================================Lineas para el weather shield=====================================================//

MPL3115A2 myPressure; //Create an instance of the pressure sensor
HTU21D myHumidity; //Create an instance of the humidity sensor
//pines digitales
unsigned long lastMillis=0;
const bool eco = true ;
const byte WSPEED = 3;
const byte RAIN = 2;
const byte STAT1 = 7;
const byte STAT2 = 8;
//pines analogicos
const byte REFERENCE_3V3 = A3;
const byte LIGHT = A1;
const byte BATT = A2;
const byte WDIR = A0;
//variables
long lastSecond; //The millis counter to see when a second rolls by
byte seconds; //When it hits 60, increase the current minute
byte minutes; //Keeps track of where we are in various arrays of data
byte minutes_10m; //Keeps track of where we are in wind gust/dir over last 10 minutes array of data
long lastWindCheck = 0;
volatile long lastWindIRQ = 0;
volatile byte windClicks = 0;
volatile float rainHour[60]; //60 floating numbers to keep track of 60 minutes of rain
String winddir = ""; // [0-360 instantaneous wind direction]
float windspeedmph = 0; // [mph instantaneous wind speed]
float humidity = 0; // [%]
float tempf = 0; // [temperature C]
float rainin = 0; // [rain inches over the past hour)] -- the accumulated rainfall in the past 60 min
volatile float dailyrainin = 0; // [rain inches so far today in local time]
//float baromin = 30.03;// [barom in] - It's hard to calculate baromin locally, do this in the agent
float pressure = 0;
//float dewptf; // [dewpoint F] - It's hard to calculate dewpoint locally, do this in the agent
float batt_lvl = 11.8; //[analog value from 0 to 1023]
float light_lvl = 455; //[analog value from 0 to 1023]
int cont=0;
String auxdirViento;
String auxvelViento;
String auxHum;
String auxTemp;
String auxLluvia;
String auxLluviad;
String auxLuz;
String auxBatt;
String auxPres;
String telegraph;
String auxTelegraph;
 int val;
 String dirGrados="";
// volatiles are subject to modification by IRQs
volatile unsigned long raintime, rainlast, raininterval, rain;
void rainIRQ()
// Count rain gauge bucket tips as they occur
// Activated by the magnet and reed switch in the rain gauge, attached to input D2
{
  raintime = millis(); // grab current time
  raininterval = raintime - rainlast; // calculate interval between this and last event
    if (raininterval > 10) // ignore switch-bounce glitches less than 10mS after initial edge
  {
    dailyrainin += 0.2794; //Each dump is 0.011" of water o 0.2794mm 
     rainHour[minutes] += 0.2794; //Increase this minute's amount of rain 
    rainlast = raintime; // set up for next event
  }
}
void wspeedIRQ()
// Activated by the magnet in the anemometer (2 ticks per rotation), attached to input D3
{
  if (millis() - lastWindIRQ > 10) // Ignore switch-bounce glitches less than 10ms (142MPH max reading) after the reed switch closes
  {
    lastWindIRQ = millis(); //Grab the current time
    windClicks++; //There is 1.492MPH for each click per second.
  }
}

//========================================Lineas para el weather shield=====================================================//


//=======================Lineas para el modem GSM====================================//

const char apn1[] = "internet";
const char user1[]= "";
const char pass1[] = "";

const char apn2[] = "bam.entelpcs.cl";
const char user2[] = "entelpcs";
const char pass2[] = "entelpcs";

const char apn3[] = "bam.clarochile.cl";
const char user3[] = "clarochile";
const char pass3[] = "clarochile";

const char apn4[] = "wap.tmovil.cl";
const char user4[] = "wap";
const char pass4[] = "wap";

// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial1
TinyGsm modem(SerialAT);
TinyGsmClient client2(modem);
PubSubClient client(client2);

//Datos Mosquitto
const char* mqttServer = "tecnoandina-server.ddns.net";
//const char* mqttServer = "iot.eclipse.org";
const int mqttPort = 1883;
const char* mqttUser = "tecnoandina"; 
const char* mqttPassWd = "kennedy5600";
String modelNumber = "TecAnd003";
String serialNumber = "85SBOB";
String status_mqtt = modelNumber + "/" + serialNumber + "/status";
String LCD = modelNumber + "/" + serialNumber + "LCD";
const char* mqttID = serialNumber.c_str();
String topic_pub= "telegraf/" +  modelNumber + "/" + serialNumber;

LiquidCrystal_I2C lcd(0x3f, 20, 4);

void setup() {

lcd.init();
lcd.backlight();
sensors6.begin(); //Temperatura
sensors7.begin(); //Temperatura
sensors9.begin(); //Temperatura

dht4.begin();
dht5.begin();

Serial.begin(115200);


//=======================Lineas para el RTC====================================
 if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2018, 4, 4, 11, 17, 0));
  }

//=======================Lineas para la uSD====================================  */


  pinMode(STAT1, OUTPUT); //Status LED Blue
  pinMode(STAT2, OUTPUT); //Status LED Green
  pinMode(WSPEED, INPUT_PULLUP); // input from wind meters windspeed sensor
  pinMode(RAIN, INPUT_PULLUP); // input from wind meters rain gauge sensor
  pinMode(REFERENCE_3V3, INPUT);
  pinMode(LIGHT, INPUT);

  myPressure.begin(); // Get sensor online
  myPressure.setModeBarometer(); // Measure pressure in Pascals from 20 to 110 kPa
  myPressure.setOversampleRate(7); // Set Oversample to the recommended 128
  myPressure.enableEventFlags(); // Enable all three pressure and temp event flags 
  myHumidity.begin();
  seconds = 0;
  lastSecond = millis();
  attachInterrupt(0, rainIRQ, FALLING);
  attachInterrupt(1, wspeedIRQ, FALLING);
  interrupts();
 

//=======================Lineas para modem GSM ====================================
  
conexion_gsm();

  
//=======================Lineas para modem GSM ====================================


mqtt_connect();


lastMillis=millis();

a=analogRead(8);
b=a;
c=a; 
delay(3000);
lcd.noBacklight();
interval = EEPROM.read(5);
}


void loop() {




////////////////////////////////////////////////////////////////////////
//


if (millis()-millis_loop>10000)
{

if (modem.isNetworkConnected()) {
    Serial.println("Conectado a la red");
}
else
{
    Serial.println("desconectado");
    conexion_gsm();
    mqtt_connect();
}


millis_loop=millis();

}


if (!client.connected())
{
mqtt_connect();
}
client.loop();



lcd.setCursor(0, 3);
lcd.print(F("                   "));
lcd.setCursor(0, 3);
lcd.print(gettime().c_str());


count_main++;


digitalWrite(STAT2, HIGH); //Blink stat LED

     //Calc the wind speed and direction every second for 120 second to get 2 minute average
    float currentSpeed = windspeedmph;
    //float currentSpeed = random(5); //For testing
    int currentDirection = get_wind_direction();
      //if(seconds_2m % 10 == 0) displayArrays(); //For testing
    if(++seconds > 59)
    {
      seconds = 0;
      if(++minutes > 59) minutes = 0;
      if(++minutes_10m > 9) minutes_10m = 0;
      rainHour[minutes] = 0; //Zero out this minute's rainfall amount
    }
  //Calc winddir
   val=get_wind_direction();
   if (val<320 && val>310) {
   winddir= "315"; 
   dirGrados="ONO, 315";  }
   if (val<253 && val>243){
   winddir= "248"; 
   dirGrados="OSO, 248";  }
   if (val<275 && val>265) {
   winddir= "270"; 
   dirGrados="Oeste, 270";  }
   if (val<298 && val>288) {
   winddir= "293"; 
   dirGrados="NNO, 293";  }
   if (val<342 && val>333) {
   winddir= "338"; 
   dirGrados="NO, 338";  }
   if (val<28 && val>18) {
   winddir= "23"; 
   dirGrados="NNE, 23";  }
   if (val==0) {
   winddir= "0"; 
   dirGrados="Norte, 0";  }
   if (val<208 && val>198) {
   winddir= "203"; 
   dirGrados="SSO, 203";  }
   if (val<230 && val>220) {
   winddir= "225"; 
   dirGrados="SO, 225";   }
   if (val<72 && val>63) {
   winddir= "68"; 
   dirGrados="ENE, 68";  }
   if ( val<50 && val>40) {
   winddir= "45"; 
   dirGrados="NE, 45"; }
   if (val<163 && val>153) {
   winddir= "158";
   dirGrados="SSE, 158";  }
   if (val<185 && val>175) {
   winddir= "180"; 
   dirGrados="Sur, 180";  }
   if (val<118 && val>108) {
   winddir= "113";
   dirGrados="ESE, 113";  }
   if (val<140 && val>130) {
   winddir= "135"; 
   dirGrados="SE, 135";  }
   if (val<95 && val>85) {
   winddir= "90"; 
   dirGrados="Este, 90";  }

  
  //Calc windspeed weather shield
  windspeedmph = get_wind_speed();


  
  //Calc humidity  weather shield
  humidity = myHumidity.readHumidity();
  
  
  //Calc tempf from pressure sensor  weather shield
  //tempf = myPressure.readTempF(); //grados fahrenheit
  tempf = myPressure.readTemp(); // Celsius
 
  
  //Total rainfall for the day is calculated within the interrupt
  //Calculate amount of rainfall for the last 60 minutes
  rainin = 0;  
  for(int i = 0 ; i < 60 ; i++)
    rainin += rainHour[i];

    
  // obtencion de la presion del modulo weather
  pressure = myPressure.readPressure();


// obtencion de temperatura y humedad DHT11///////////////
float h5 = dht5.readHumidity();
float t5 = dht5.readTemperature();
float h4 = dht4.readHumidity();
float t4 = dht4.readTemperature();


// obtencion de temperaturas DS18B20 ////////////////////
sensors6.requestTemperatures();
float ds18_temp6 = sensors6.getTempCByIndex(0) - offset;


sensors7.requestTemperatures();
float ds18_temp7 = sensors7.getTempCByIndex(0) - offset;


sensors9.requestTemperatures();
float ds18_temp9 = sensors9.getTempCByIndex(0) - offset;

// obtencion valor piranometro ////////////////////
pir= analogRead(12);
pir = (pir*2000)/1024; 

//calculo de corriente ACS712
Amp = (analogRead(8) /1024.0)*5000;
Amp=((Amp-2500)/66)*(-1);

//medicion de voltaje de paneles solares
volt_measure();

//medicion de corriente MAX471 (peltier)
//q = (analogRead(13))*5/1024;
q = analogRead(14)*5/1024.0;

//medicion de voltaje MAX471 (peltier)
//v = (analogRead(14))*5/1024;
v = analogRead(13)*13.98/564;

//
 aux_ds18_temp6=aux_ds18_temp6+ds18_temp6;
 aux_ds18_temp7=aux_ds18_temp7+ds18_temp7;
 aux_ds18_temp9=aux_ds18_temp9+ds18_temp9;
 aux_WS=aux_WS+windspeedmph;
 aux_WD=aux_WD+winddir.toFloat();
 aux_pressure=aux_pressure+pressure;
 aux_humidity=aux_humidity+humidity;
 aux_tempf=aux_tempf+tempf;
 aux_h4=aux_h4+h4;
 aux_h5=aux_h5+h5;
 aux_t4=aux_t4+t4;
 aux_t5=aux_t5+t5;
 aux_Amp=aux_Amp+Amp;
 aux_ar=aux_ar+ar;
 aux_v=aux_v+ar2;
 aux_pir=aux_pir+pir;
 aux_q1=aux_q1+q;
 aux_peltv=aux_peltv+v;
 aux_peltc=aux_peltc+q;

//

//...



if (millis()-lastMillis>(5*60000))
//if (millis()-lastMillis>(30000))
{


ds18_temperatura6= (String) (aux_ds18_temp6/count_main);
ds18_temperatura7=  (String) (aux_ds18_temp7/count_main); 
ds18_temperatura9= (String) (aux_ds18_temp9/count_main); 
WS= (String) (aux_WS/count_main); 
WD= (String) (aux_WD/count_main); 
presion= (String) (aux_pressure/count_main); 
humedad= (String) (aux_humidity/count_main); 
temperatura= (String) (aux_tempf/count_main); 
humedad4= (String) (aux_h4/count_main); 
humedad5= (String) (aux_h5/count_main); 
temperatura4= (String) (aux_t4/count_main); 
temperatura5= (String) (aux_t5/count_main); 
corriente_load= (String) (aux_Amp/count_main); 
Pan_propio= (String) (aux_v/count_main); 
Pan_externo= (String) (aux_ar/count_main); 
Piranometro=(String) (aux_pir/count_main); 
peltier_v=(String)(aux_peltv/count_main);
peltier_c=(String)(aux_peltc/count_main);


Serial.print("temp DS18-1: ");
Serial.println(ds18_temperatura6.c_str());
Serial.print("temp DS18-2: ");
Serial.println(ds18_temperatura7.c_str());
Serial.print("temp DS18-3: ");
Serial.println(ds18_temperatura9.c_str());
Serial.print("vel viento: ");
Serial.println(WS.c_str());
Serial.print("dir viento: ");
Serial.println(WD.c_str());
Serial.print("humedad weather shield: ");
Serial.println(humedad.c_str());
Serial.print("temp weather shield: ");
Serial.println(temperatura.c_str());
Serial.print("presion weather shield: ");
Serial.println(presion.c_str());
Serial.print("humedad DHT11-1: ");
Serial.println(humedad4.c_str());
Serial.print("humedad DHT11-2: ");
Serial.println(humedad5.c_str());
Serial.print("temp DHT11-1: ");
Serial.println(temperatura4.c_str());
Serial.print("temp DHT11-2: ");
Serial.println(temperatura5.c_str());
Serial.print("corriente resistencia: ");
Serial.println(corriente_load.c_str());
//Serial.println(Amp);
//Serial.println(analogRead(8));
Serial.print("Voltaje propio: ");
Serial.println(Pan_propio.c_str());
//Serial.println(ar2);
Serial.print("Voltaje externo: ");
Serial.println(Pan_externo.c_str());
//Serial.println(ar);
Serial.print("Piranometro: ");
Serial.println(Piranometro.c_str());
Serial.print("corriente peltier: ");
//Serial.println(peltier_c.c_str());
Serial.println(q);
Serial.print("voltaje peltier: ");
//Serial.println(peltier_v.c_str());
Serial.println(v);
Serial.print("count: ");
Serial.println(count_main);



//************************************************TELEGRAPH**************************************************************
telegraph="Monitor_panel_solar,modelNumber=TecAnd003,serialNumber=85SBOB,verSoft=0.1 ";
//************************************************TELEGRAPH**************************************************************

 
telegraph= telegraph+"corriente="+corriente_load+",DS18B20_1_temp="+ds18_temperatura6+",DS18B20_2_temp="+ds18_temperatura7+",DS18B20_3_temp="+ds18_temperatura9+",windspeed="+WS+",windDir="+WD+",humedadWeather="+humedad+",tempWeather="+temperatura+",pressWeather="+presion+",DHT11_1_Hum="+humedad4+",DHT11_2_hum="+humedad5+",DHT11_1_Temp="+temperatura4+",DHT11_2_Temp="+temperatura5+",Piranom="+Piranometro+",Volt_ext_panel="+Pan_externo+",Volt_panel_propio="+Pan_propio+",Voltajepeltier="+(String)v+",Corrientepeltier="+(String)q;


client.publish(topic_pub.c_str(),telegraph.c_str());
//client.publish("status/gsm/tecnoandina","hola");

cuenta++;
countx=(String)cuenta;
client.publish("status/cuenta/tecnoand",countx.c_str());


Serial.println();
 Serial.println();
 Serial.println();
 Serial.println();
 Serial.println();
 Serial.println();
 Serial.println();
 Serial.println();
//




lastMillis=millis();
count_main=0;
sd_freq++;

aux_ds18_temp6=0;
aux_ds18_temp7=0;
aux_ds18_temp9=0;
aux_WS=0;
aux_WD=0;
aux_pressure=0;
aux_humidity=0;
aux_tempf=0;
aux_h4=0;
aux_h5=0;
aux_t4=0;
aux_t5=0;
aux_Amp=0;
aux_ar=0;
aux_ar2=0;
aux_pir=0;
aux_v=0;

}

if (sd_freq==3)
{

Serial.println("grabando uSD...");
write_SD();
sd_freq=0;

}


}// end loop*************************************************************************************************************


//********************************************CALLBACK********************************************************************

void callback(char* topic, byte* payload, unsigned int length)

{

  
char mensaje[length];
String TOPic= topic;
String cargaUtil = String((char*)payload);



if(TOPic=="gsm_off")//*********************************************************************************
                

{
 
 for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

 if(!(strncmp(mensaje,"off",3)))
  {
    Serial.println("apagando modem...");
    delay(2000);
    modem.radioOff();
  }
}



if(TOPic=="TecAnd003/85SBOB/LCD")
{

for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

 if(!(strncmp(mensaje,"on",2)))
  {
   
   lcd.backlight();

  }

  if(!(strncmp(mensaje,"off",3)))
  { 
  

  lcd.noBacklight();
  
  }

}


if(TOPic=="TecAnd003/85SBOB/intervalo")//*********************************************************************************
                

{

  interval=cargaUtil.toInt();
 EEPROM.write(5,cargaUtil.toInt());
 String publicar= "Recibido intervalo = "+(String)interval;
  client.publish("TecAnd003/85SBOB/status_intervalo",publicar.c_str());
}
    
}

void mqtt_connect()
{
//********************LOOP MQTT******************************
//***********************************************************
    lcd.setCursor(0, 3);
    lcd.print(F("                    "));
    lcd.setCursor(0, 3);
    lcd.print(F("Connecting MQTT"));
    
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  if (!client.connected()) {
    Serial.print("Connecting to MQTT ");
    Serial.println(mqttServer);

   if (client.connect(mqttID,mqttUser,mqttPassWd, status_mqtt.c_str(),1,1,"OFFLINE")) {

   client.publish(status_mqtt.c_str(),"ONLINE",true);
   client.subscribe("TecAnd003/85SBOB/intervalo");
   client.subscribe("gsm_off");
   client.subscribe("TecAnd003/85SBOB/LCD");

      Serial.println("Connected to MQTT Broker");
      Serial.println("----------------------------------------");
      lcd.setCursor(0, 3);
      lcd.print(F("                    "));
      lcd.setCursor(0, 3);
      lcd.print(F("MQTT OK"));
      delay(1000);

    }//End If
    else {
      Serial.print("failed with state: ");
      Serial.println(client.state());
      lcd.setCursor(0, 3);
      lcd.print(F("                    "));
      lcd.setCursor(0, 3);
      lcd.print(F("Failed"));
      delay(2000);
    }//End Else


}//END While

}

void reconnect() {

     Serial.print("reconnecting");
if (client.connect(mqttID,mqttUser,mqttPassWd, status_mqtt.c_str() ,1,1,"OFFLINE")) {

   client.publish(status_mqtt.c_str(),"ONLINE",true);
   client.subscribe("test_gsm_yulman");
   client.subscribe("gsm_off");

      }
     else {
      Serial.print("failed with state: ");
      Serial.println(client.state());
      lcd.setCursor(0, 3);
      lcd.print(F("                    "));
      lcd.setCursor(0, 3);
      lcd.print(F("Failed"));
      delay(2000);
    }//End Else
   
      delay(500);
      }


  float get_battery_level()
{
  float operatingVoltage = analogRead(REFERENCE_3V3);
  float rawVoltage = analogRead(BATT);
  operatingVoltage = 3.30 / operatingVoltage; //The reference voltage is 3.3V
  rawVoltage = operatingVoltage * rawVoltage; //Convert the 0 to 1023 int to actual voltage on BATT pin
  rawVoltage *= 4.90; //(3.9k+1k)/1k - multiple BATT voltage by the voltage divider to get actual system voltage
  return(rawVoltage);
}
//Returns the instataneous wind speed
float get_wind_speed()
{
  float deltaTime = millis() - lastWindCheck; //750ms
  deltaTime /= 1000.0; //Covert to seconds
  float windSpeed = (float)windClicks / deltaTime; //3 / 0.750s = 4
  windClicks = 0; //Reset and start watching for new wind
  lastWindCheck = millis();
  windSpeed *= 2.4; //4 * 1.492 = 5.968MPH; para km/h se debe multiplicar por 2.4 en lugar de 1.492
  return(windSpeed);
}
//Read the wind direction sensor, return heading in degrees
int get_wind_direction() 
{
 unsigned int adc;
  adc = analogRead(WDIR); // get the current reading from the sensor
  // The following table is ADC readings for the wind direction sensor output, sorted from low to high.
  // Each threshold is the midpoint between adjacent headings. The output is degrees for that ADC reading.
  // Note that these are not in compass degree order! See Weather Meters datasheet for more information.
 if (adc < 380) return (315);
  if (adc < 393) return (247.5);
  if (adc < 414) return (270);
  if (adc < 456) return (292.5);
  if (adc < 508) return (337.5);
  if (adc < 551) return (22.5);
  if (adc < 615) return (0);
  if (adc < 680) return (202.5);
  if (adc < 746) return (225);
  if (adc < 801) return (67.5);
  if (adc < 833) return (45);
  if (adc < 878) return (157);
  if (adc < 913) return (180);
  if (adc < 940) return (112.5);
  if (adc < 967) return (135);
  if (adc < 990) return (90);
  return (-1); // error, disconnected?
}


String gettime()
{
 DateTime now = rtc.now();
  String tiempo;
  tiempo= (String)now.hour();
  tiempo+=":";
 if(now.minute()<10)
  {
    tiempo=tiempo+"0"+(String)now.minute();
  }
  else
  {
    tiempo+=(String)now.minute();
  }
  
  tiempo+=":";

  if(now.second()<10)
  {
    tiempo=tiempo+"0"+(String)now.second();
  }
  else
  {
    tiempo+=(String)now.second();
  }
  
 
 
  Serial.println(tiempo.c_str());

  return tiempo;
}


 void conexion_gsm()
  {
bool Ready=1;

lcd.init();

lcd.clear();
lcd.setCursor(0, 0);
lcd.print(F("TecnoAndina SpA."));
lcd.setCursor(0, 1);
lcd.print("Paneles solares");
lcd.setCursor(0, 2);
lcd.print(F("Date :"));
lcd.print(__DATE__);

while (Ready==1)
{
// Set GSM module baud rate
SerialAT.begin(115200);
delay(3000);

Serial.println("Initializing modem...");
lcd.clear();
lcd.setCursor(0, 0);
lcd.print(F("Initializing modem"));
modem.restart();

String modemInfo = modem.getModemInfo();
Serial.print("Modem: ");
Serial.println(modemInfo);

// Unlock your SIM card with a PIN
//modem.simUnlock("1234");

lcd.setCursor(0, 1);
lcd.print(F("Waiting Network"));
Serial.print("Waiting for network...");

if (modem.waitForNetwork()) {
lcd.print(F(" OK"));
if (modem.gprsConnect(apn1, user1, pass1)) {
lcd.setCursor(0, 2);
lcd.print(F(" "));
lcd.setCursor(0, 2);
lcd.print(F("Connecting APN OK"));
delay(2000);
lcd.setCursor(0, 2);
lcd.print(F("                   "));
lcd.setCursor(0, 2);
lcd.print(F("conexion: Wom"));

Ready=0;
Serial.println("conectado a wom"); 
}

else if (modem.gprsConnect(apn2, user2, pass2)) {
lcd.setCursor(0, 2);
lcd.print(F(" "));
lcd.setCursor(0, 2);
lcd.print(F("Connecting APN OK"));
delay(2000);
lcd.setCursor(0, 2);
lcd.print(F("                   "));
lcd.setCursor(0, 2);
lcd.print(F("conexion: Entel"));

Ready=0;
Serial.println("conectado a entel"); 
}

else if (modem.gprsConnect(apn3, user3, pass3)) {
lcd.setCursor(0, 2);
lcd.print(F(" "));
lcd.setCursor(0, 2);
lcd.print(F("Connecting APN OK"));
delay(2000);
lcd.setCursor(0, 2);
lcd.print(F("                   "));
lcd.setCursor(0, 2);
lcd.print(F("conexion: Claro"));

Ready=0;
Serial.println("conectado a claro"); 
}

else if (modem.gprsConnect(apn4, user4, pass4)) {
lcd.setCursor(0, 2);
lcd.print(F(" "));
lcd.setCursor(0, 2);
lcd.print(F("Connecting APN OK"));
delay(2000);
lcd.setCursor(0, 2);
lcd.print(F("                   "));
lcd.setCursor(0, 2);
lcd.print(F("conexion: Movistar"));

Ready=0;
Serial.println("conectado a movistar"); 
}
else
{
lcd.setCursor(0, 2);
lcd.print(F(" "));
lcd.setCursor(0, 2);
lcd.print(F("APN fail !!!"));
}
}
else
{
lcd.setCursor(0, 1);
lcd.print(F(" "));
lcd.setCursor(0, 1);
lcd.print(F("Network Fail"));
}

}
  }


  
String ACS_712()
 
{
String current="";




//Amp = (analogRead(8) /1024.0)*5000;
//Amp=((Amp-2500)/66);


Amp = analogRead(8);

return current= ",Corriente="+(String)Amp;

}




String volt_measure()
{
  String volt="";
//
ar= (analogRead(15))*12.20/307;

ar2= (analogRead(10))*13.97/818;


volt= ",Volt_ext_panel="+(String)ar+",Volt_panel_propio="+(String)ar2;

return volt;
}




void write_SD()
{
if (!SD.begin(8)) {
 
 
  
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.

  myFile = SD.open("test.txt", FILE_WRITE);
   delay(100);
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
  


myFile.println("");

//myFile.print("Hora: ");
myFile.print(gettime_SD().c_str());
myFile.print(";");

//myFile.print("temp DS18-1: ");
myFile.print(ds18_temperatura6.c_str());
myFile.print(";");
//myFile.print("temp DS18-2: ");
myFile.print(ds18_temperatura7.c_str());
myFile.print(";");
//myFile.print("temp DS18-3: ");
myFile.print(ds18_temperatura9.c_str());
myFile.print(";");
//myFile.print("Velocidad viento: ");
myFile.print(WS.c_str());
myFile.print(";");
//myFile.print("Direccion viento: ");
myFile.print(WD.c_str());
myFile.print(";");
//myFile.print("Humedad weather: ");
myFile.print(humedad.c_str());
myFile.print(";");
//myFile.print("Temp weather: ");
myFile.print(temperatura.c_str());
myFile.print(";");
//myFile.print("Presion weather: ");
myFile.print(presion.c_str());
myFile.print(";");
//myFile.print("Humedad DHT11_1: ");
myFile.print(humedad4.c_str());
myFile.print(";");
//myFile.print("Humedad DHT11_2: ");
myFile.print(humedad5.c_str());
myFile.print(";");
//myFile.print("Temp DHT11_1: ");
myFile.print(temperatura4.c_str());
myFile.print(";");
//myFile.print("Temp DHT11_2: ");
myFile.print(temperatura5.c_str());
myFile.print(";");
//myFile.print("Corriente: ");
myFile.print(corriente_load.c_str());
myFile.print(";");
//myFile.print("Volt_pan_ext: ");
myFile.print(Pan_externo.c_str());
myFile.print(";");
//myFile.print("Volt_pan_propio: ");
myFile.print(Pan_propio.c_str());
myFile.print(";");
//myFile.print("Piranometro: ");
myFile.print(Piranometro.c_str());
myFile.print(";");
//myFile.print("corriente pelt: ");
myFile.print(q);
myFile.print(";");
//myFile.print("voltaje pelt: ");
myFile.print(v);

    //close the file:
    myFile.close();
    Serial.println("done.");
    delay(1000);
 
  }



  myFile = SD.open("test.txt");
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:

    // close the file:
    myFile.close();

}
}


String gettime_SD()
{
 DateTime now = rtc.now();
  String tiempo;
  
  tiempo=(String)now.year();
  tiempo=tiempo+(String)now.month();
  tiempo=tiempo+(String)now.day();
  tiempo=tiempo+(String)now.hour();
 
 
  //tiempo+=":";
 if(now.minute()<10)
  {
    tiempo=tiempo+"0"+(String)now.minute();
  }
  else
  {
    tiempo+=(String)now.minute();
  }
  
  //tiempo+=":";

  if(now.second()<10)
  {
    tiempo=tiempo+"0"+(String)now.second();
  }
  else
  {
    tiempo+=(String)now.second();
  }
  

 
 
  Serial.println(tiempo.c_str());
  delay(2000);
  return tiempo;
}